## Horse Distro Builder

Horse Distro Builder is a tool to make remastered Debian-live builds.  

Destined to research and tests, it should not be used in production environments.  

This use Debian GNU/Linux programs.  

This is provided "AS IS", without warranty of any kind.  

---

Copyright (c) 2023-2024 Raymond Risko
